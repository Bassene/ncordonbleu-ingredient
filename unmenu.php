<?php
include ('includes/header.php'); 
include ('config/database.php');






?>

<aside class="sidebar trans-0-4">
<!-- Button Hide sidebar -->
<button class="btn-hide-sidebar ti-close color0-hov trans-0-4"></button>



<!-- - -->
<div class="gallery-sidebar t-center p-l-60 p-r-60 p-b-40">
<!-- - -->
<h4 class="txt20 m-b-33">
Gallery
</h4>

<!-- Gallery -->
<div class="wrap-gallery-sidebar flex-w">
<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-01.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-01.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-02.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-02.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-03.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-03.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-05.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-05.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-06.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-06.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-07.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-07.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-09.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-09.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-10.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-10.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-11.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-11.jpg" alt="GALLERY">
</a>
</div>
</div>
</aside>


<!-- Title Page -->
<section class="bg-title-page flex-c-m p-t-160 p-b-80 p-l-15 p-r-15" style="background-image: url(images/bg-title-page-02.jpg);">
<h2 class="tit6 t-center">
Reservation
</h2>
</section>


<body class="animsition">

<?php
// echo $_SESSION['mail'];
$sql = "SELECT * FROM menus WHERE id = :idmenu";
if($stmt = $pdo->prepare($sql)){
// Bind variables to the prepared statement as parameters
$stmt->bindParam(":idmenu", $param_id);

// Set parameters
$param_id = $_GET['idmenu'];

// Attempt to execute the prepared statement
if($stmt->execute()){
if($stmt->rowCount() == 1){
/* Fetch result row as an associative array. Since the result set
contains only one row, we don't need to use while loop */
$row = $stmt->fetch(PDO::FETCH_ASSOC);

// Retrieve individual field value
$nommenu = $row["nommenu"];
$nomimage = $row["nomimage"];
$ladate = $row["ladate"];
$preparations = $row["preparatione"];
$ingredient = $row["ingredient"];


} else{
// URL doesn't contain valid id parameter. Redirect to error page
// header("location: error.php");
exit();
}

} else{
echo "Oops! Something went wrong. Please try again later.";
}
}
$bdd = $pdo->query("SELECT menus.nomimage, ingredientmenus.nomingredient
FROM menus
INNER JOIN ingredientmenus
ON menus.id = ingredientmenus.idmenu WHERE ingredientmenus.idmenu  = $param_id ");
$join = $bdd->fetchAll();
// Close statement
unset($stmt);
?>




<!-- Title Page -->
<section class="bg-title-page flex-c-m p-t-160 p-b-80 p-l-15 p-r-15" style="background-image: url(images/bg-title-page-03.jpg);">
<h2 class="tit6 t-center">
Menu
</h2>
</section>


<!-- Content page -->
<section>



<div class="container">
<div class="row ">
<div class="col-md-8 col-lg-9">
<div class="p-t-80 p-b-124 bo5-r p-r-50 h-full p-r-0-md bo-none-md">
<!-- Block4 -->
<div class="blo4 p-b-63">
	<!-- - -->
	<div class="pic-blo4 hov-img-zoom bo-rad-10 pos-relative">
		<a href="blog-detail.html">
			<img src="images/<?php echo $nomimage; ?>" alt="IMG-BLOG">
		</a>

		<div class="date-blo4 flex-col-c-m">
			<span class="txt30 m-b-4">
            <?php echo "".date("d",strtotime($ladate)); ?>
			</span>

			<span class="txt31">
            <?php echo "".date("m-Y",strtotime($ladate)); ?>
			</span>
		</div>
	</div>
	<div class="text-blo4 p-t-33">
		<h4 class="p-b-16">
			<a href="" class="tit9"><?php echo $nommenu; ?></a>
		</h4>

		<div class="txt32 flex-w p-b-24">
			<span>
				by Admin
				<span class="m-r-6 m-l-4">|</span>
			</span>

			<span>
            <?php echo date("d-m-Y", strtotime($ladate));  ?>
				<span class="m-r-6 m-l-4">|</span>
			</span>

			
		</div>

		  <div class="container">

  <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Liste des ingredients</h1>

  <hr class="mt-2 mb-5">
  <div class="row text-center text-lg-left">
<?php 
           foreach ($join as $value):  
		  ?> 
      <style type="text/css">
			.img-thumbnail{
				height: 150px;
		    width: 150px ;
		    
			}

		</style>
		  	<div class="col-lg-3 col-md-4 col-6">
		  		<strong style="text-align: center;"> <?php echo ucfirst(str_replace(".jpg", "    ",  $value['nomingredient'])); ?></strong><br>
		  <img class="img-fluid img-thumbnail" src="ingredients/<?= $value['nomingredient']; ?>">
           
         </div>
         <?php endforeach ?>
          </div>

       </div>
	</div>

        <p>
        <strong>Préparations:</strong>
		</p>
        <p>
        <?php echo $preparations; ?>
		</p>
</div>
	<!-- - -->
	

		
		
   

</div>
</div>
<!-- droite -->
<?php
include('droite.php');
?>
</div>
</div>
</section>


<!-- Footer -->
<?php
include('includes/footer.php');
?>
