<?php include ('includes/header.php'); ?>
<?php include ('test.php'); ?>
<?php include_once 'fonction/rechercher_ingredient.php'; ?>
<?php 
/*$sql = 'SELECT COUNT(*) AS nb FROM ingredients';
    $result = $bdd->query($sql);
    $columns = $result->fetch();
    $nb = $columns['nb'];
    $input = 0;
    while ($input <= $nb) {
      $input++; ?>
      <input type="text" name="quantity" class="k" style="background-color: black;">
  <?php     
    }*/
 ?>
<!-- Sidebar -->
<aside class="sidebar trans-0-4">
<!-- Button Hide sidebar -->
<button class="btn-hide-sidebar ti-close color0-hov trans-0-4"></button>



<!-- - -->
<div class="gallery-sidebar t-center p-l-60 p-r-60 p-b-40">
<!-- - -->
<h4 class="txt20 m-b-33">
Gallery
</h4>

<!-- Gallery -->
<div class="wrap-gallery-sidebar flex-w">
<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-01.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-01.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-02.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-02.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-03.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-03.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-05.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-05.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-06.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-06.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-07.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-07.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-09.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-09.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-10.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-10.jpg" alt="GALLERY">
</a>

<a class="item-gallery-sidebar wrap-pic-w" href="images/photo-gallery-11.jpg" data-lightbox="gallery-footer">
<img src="images/photo-gallery-thumb-11.jpg" alt="GALLERY">
</a>
</div>
</div>
</aside>


<!-- Title Page -->
<section class="bg-title-page flex-c-m p-t-160 p-b-80 p-l-15 p-r-15" style="background-image: url(images/bg-title-page-02.jpg);">
<h2 class="tit6 t-center">
Reservation
</h2>
</section>


<!-- Reservation -->
<section class="section-reservation bg1-pattern p-t-100 p-b-113">
<div class="container">
<div class="row">
<div class="col-lg-12 p-b-30">
<div class="t-center">
	<span class="tit2 t-center">
		Reservation
	</span>

	<h3 class="tit3 t-center m-b-35 m-t-2">
		Book table
	</h3>
</div>

<form class="wrap-form-reservation size22 m-l-r-auto" action="" method="post" enctype="multipart/form-data" name="fo">
	<div class="row">

		

		<div class="col-md-4">
			
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<!-- Name -->
			<span class="txt9">
				Name
			</span>

			<div class="wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
				<input class="bo-rad-10 sizefull txt10 p-l-20" type="text" name="menu" placeholder="Name">
			</div>
		</div>

	</div>

    <div class="row">
		<div class="col-md-4">
			<!-- Name -->
			<span class="txt9">
				Images
			</span>

			<div class="wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
				<input class="bo-rad-10 sizefull txt10 p-l-20" type="file" name="image">
			</div>
		</div>

	</div>
	

    

    <div class="row">
        <div class="col-12">
        <!-- Message -->
        <span class="txt9">
            Préparation
        </span>
        <textarea class="bo-rad-10 size35 bo2 txt10 p-l-20 p-t-15 m-b-10 m-t-3" name="preparation" placeholder="La préparation"></textarea>
    </div>
    </div>
  <style type="text/css">
  	.img-thumbnail{
      height: 150px ;
        width: 90% ;
        transition:all 0.3s ease;
        padding-bottom: 15%;
    }
  	.k{
  		height: 19px ;
      width: 90% ;
     
  	}
   @media all  and (max-width: 1232px){
    .img-thumbnail{
       width: 80% ;
    }
      .k{
         width: 80% ;
      }
   }
    @media all and(max-width: 942px) {
    .img-thumbnail{
       width: 80% ;
    }
      .k{
         width: 80% ;
      }
   }
   
   
  </style>
 <div class="container">

  <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Liste des ingredients</h1>
  <br>
  <?php include_once 'search.php'; ?>
  
  <hr>
  <div class="row text-center text-lg-left">
  	
<?php foreach ($donnees as $ingredients) : ?>

    <div class="col-lg-3 col-md-4 col-6">
        <div class="d-block mb-4 h-200">
      <label>
    	<input type="checkbox" id="img" name="ingredient[]" value="<?= $ingredients['image']; ?>">
      <strong><?php echo  ucfirst($ingredients['nom']); ?></strong>

      </label>
            <img class="img-fluid img-thumbnail" src="ingredients/<?= $ingredients['image']; ?>" alt=""  >
           
            <input type="text"
                        id=""
                        name="quantity[]" class="k" />
       </div>
         
    </div>
     <?php endforeach ?>

  	

  </div>

</div>
<!-- /.container -->

<br>
	<div class="wrap-btn-booking flex-c-m m-t-6">
		<!-- Button3 -->
		<input type="submit" name="submit" class="btn3 flex-c-m size13 txt11 trans-0-4">
		</input>
	</div>
</form>
</div>
</div>
<?php if (isset($mg)) {
	echo $mg;
} ?>

</div>
</section>
<!-- Page Content -->


<?php include ('includes/footer.php'); ?>